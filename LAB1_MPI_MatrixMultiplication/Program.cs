﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPI;

namespace LAB1_MPI_MatrixMultiplication
{
    class Program
    {
        static int matrix_size = 512;
        static void Main(string[] args)
        {
            using (new MPI.Environment(ref args))
            {
                Intracommunicator comm = Communicator.world;
                
                if(args.Length == 1)
                {
                    matrix_size = Int16.Parse(args[0]);
                }
                
                if(comm.Rank == 0)
                {
                    Console.WriteLine("Matrix size: {0}", matrix_size);
                }

                //Define variables for matrix
                double[] matrixA = null;
                double[] matrixB = null;
                double[] matrixC = null;
                double[] matrixPartA = null;
                double[] matrixPartC = null;

                //Main Process init matrix for source matrix and result matrix;
                if(comm.Rank == 0)
                {
                    matrixA = new double[matrix_size * matrix_size];
                    matrixC = new double[matrix_size * matrix_size];
                    matrixB = new double[matrix_size * matrix_size];
                    
                    var random = new System.Random();
                    //Init matrix A and B elements with random value
                    for(var i = 0; i < matrix_size; i++)
                    {
                        matrixA[i] = random.Next(1, 100);
                        matrixB[i] = random.Next(1, 100);
                        matrixC[i] = 0;
                    }
                }

                //Part of source and result matrix init each Process
                matrixPartA = new double[matrix_size * matrix_size / comm.Size];
                matrixPartC = new double[matrix_size * matrix_size / comm.Size];

                //Brodcast send matrix B from zero rank process to each other process;
                comm.Broadcast<Double>(ref matrixB, 0);

                //Destribute matrix to Processes 
                if (comm.Rank == 0)
                {
                    //Create array of array to ditribute part of array to Processors
                    var distributionArray = new double[comm.Size][];
                    
                    //TODO distribute matrixA to subarrays;


                    //Send distributed arrays to Processors
                    comm.Scatter<Double[]>(distributionArray);  
                }
                else
                {
                    matrixPartA = comm.Scatter<Double[]>(0);
                    Console.WriteLine("Rank {0} recived {1}", comm.Rank, matrixPartA);
                }

            }
        }
    }
}
